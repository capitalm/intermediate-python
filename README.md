# Python Intermediate Level Tips

## Installation

```bash
sudo pip3 install jupyter
git clone https://gitlab.com/capitalm/intermediate-python.git
cd intermediate-python
```

## Running

```bash
jupyter notebook
```